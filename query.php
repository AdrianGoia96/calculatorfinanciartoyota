<?php
include "DB.php";

$model = $_GET["model"];
$dealer = $_GET["dealer"];
$nume = $_GET["nume"];
$telefon = $_GET["telefon"];
$email = $_GET["email"];
$peste_18 = $_GET["peste18"] ? 1 : 0;
$acord_prim_info = $_GET["acord"] ? 1 : 0;
$avans_euro = $_GET["avans_euro"];
$perioada = $_GET["perioada"];
$rata = $_GET["rata"];
$credit = $_GET["credit"];
$dobanda = $_GET["dobanda"];
$com_analiza = $_GET["com_analiza"];
$com_lunar = $_GET["com_lunar"];
$dae = $_GET["dae"];
$total = $_GET["total"];


$check = "Select count(*) From calculator_financiar_toyota Where email = '{$email}'";
$check = $conn->query($check);
$check = $check->fetch_assoc();
$check = $check["count(*)"];

if($check === "0"){
    $sql = "INSERT INTO calculator_financiar_toyota(model,dealer,nume,telefon,email,acord_varsta,acord_prim_info,avans_euro,perioada,rata,credit,dobanda,com_analiza,dae,total) VALUES ('{$model}','{$dealer}','{$nume}','{$telefon}','{$email}','{$peste_18}','{$acord_prim_info}','{$avans_euro}','{$perioada}','{$rata}','{$credit}','{$dobanda}','{$com_analiza}','{$dae}','{$total}')";
    $result = $conn->query($sql);
}else{
    $sql = "Select * From calculator_financiar_toyota Where email = '{$email}'";
    $result = $conn->query($sql);
    while($row = $result->fetch_assoc()){
        $rows[] = $row;
    }
    $rows = $rows[0];
    if(is_null($rows['avans_euro']) && is_null($rows['perioada']) && is_null($rows['rata']) && is_null($rows['credit']) && is_null($rows['dobanda']) && is_null($rows['com_analiza']) && is_null($rows['dae']) && is_null($rows['total'])) {
        $sql = "UPDATE calculator_financiar_toyota SET model= '{$model}', avans_euro = '{$avans_euro}', perioada = '{$perioada}', rata = '{$rata}', credit = '{$credit}', dobanda = '{$dobanda}', com_analiza = '{$com_analiza}', dae = '{$dae}', total = '{$total}' Where email = '{$email}'";
        $result = $conn->query($sql);
    }else{
        die("Mai usor cu spam-ul prietene!");
    }
}


$dealeri = array(
    'Selectati un dealer' => 'Selectati un dealer',
    'TOYOTA BACAU - Auto Best Center' => array('vanzari@bacau.toyota.ro'),
    'TOYOTA BAIA MARE - Land Motors' => array('vanzari@baiamare.toyota.ro'),
    'TOYOTA BISTRITA - Mondial Motors' => array('vanzari@bistrita.toyota.ro'),
    'TOYOTA BRASOV - Motors Management' => array('vanzari@brasov.toyota.ro'),
    'TOYOTA BRAILA - Estima Motors' => array('vanzari@braila.toyota.ro'),
    'TOYOTA BUCURESTI NORD - Inchcape Motors' => array('vanzari@bucurestinord.toyota.ro'),
    'TOYOTA BUCURESTI SUD -Toyo Motor Center' => array('vanzari@bucurestisud.toyota.ro'),
    'TOYOTA BUCURESTI VEST - Kaizen Auto' => array('vanzari@bucurestivest.toyota.ro'),
    'TOYOTA BUZAU - Rol Car Motors' => array('vanzari@buzau.toyota.ro'),
    'TOYOTA CLUJ NAPOCA - Profi Auto' => array('vanzari@cluj.toyota.ro'),
    'TOYOTA CONSTANTA - Motor Expert' => array('vanzari@constanta.toyota.ro'),
    'TOYOTA CRAIOVA - Top Serv Motors' => array('vanzari@craiova.toyota.ro'),
    'TOYOTA FOCSANI - Auto Best Center' => array('vanzari@focsani.toyota.ro'),
    'TOYOTA IASI - Mega Auto' => array('vanzari@iasi.toyota.ro'),
    'TOYOTA ODORHEI - Euro Car Trading' => array('vanzari@harghita.toyota.ro'),
    'TOYOTA ORADEA - Car Select Mobility' => array('vanzari@oradea.toyota.ro'),
    'TOYOTA PITESTI - Next Automobile' => array('vanzari@pitesti.toyota.ro'),
    'TOYOTA PLOIESTI - Arena Auto' => array('vanzari@ploiesti.toyota.ro'),
    'TOYOTA RAMNICU VALCEA - Next Automobile' => array('vanzari@valcea.toyota.ro'),
    'TOYOTA SIBIU - Son Motors' => array('vanzari@sibiu.toyota.ro'),
    'TOYOTA SUCEAVA - Revlaco Motors' => array('vanzari@suceava.toyota.ro'),
    'TOYOTA TARGU MURES - Euro Car Trading' => array('vanzari@mures.toyota.ro'),
    'TOYOTA TIMISOARA - New Wavemotors' => array('vanzari@timisoara.toyota.ro'),
);

$adresa = $dealeri[$dealer];


require 'lib/PHPMailerAutoload.php';

$mail = new PHPMailer;

// SMTP
$mail->IsSMTP();         // enable SMTP authentication
$mail->SMTPAuth = true;
$mail->Host       = "smtp.sendgrid.net"; // sets the SMTP server
$mail->Port       = 587;                    // set the SMTP port for the GMAIL server
$mail->Username   = "trom"; // SMTP account username
$mail->Password   = "Toyota@2017";        // SMTP account password

$mail->From = 'corolla2016@toyotamea.ro';
$mail->FromName = 'Toyota Romania';

/*$mail->addAddress($adresa[0]);

$mail->addAddress("customer.satisfaction@toyota.ro");*/

/*$mail->addBCC("adrian.goia@centrade-cheil.com");
$mail->addBCC("rebeca.penes@centrade-cheil.com");*/

$mail->isHTML(true);

$mail->Subject = 'Solicitare Oferta';

$mail->CharSet = 'UTF-8';

$rata = number_format($rata,2,",","");
$credit = number_format($credit-$com_analiza,2,",","");
$total = number_format($total,2,",","");
$dae = number_format($dae,2,",","");
$echipare = explode("_",$model)[1];
$model = explode("_",$model)[0];
$mail->Body    = 	'Nume: ' . $nume . '<br/>' .
    'Telefon: ' . $telefon . '<br/>' .
    'Email: ' . $email . '<br/>' .
    'Model: ' . $model . '<br/>' .
    'Echipare: ' . $echipare . '<br/>' .
    'Dealer: ' . $dealer . '<br/>' .
    'Avans: ' . $avans_euro . ' Euro <br/>' .
    'Perioada: ' . $perioada . ' Luni <br/>' .
    'Rata: ' . $rata . ' Lei <br/>' .
    'Credit: ' . $credit . ' Lei <br/>' .
    'Dobanda: ' . $dobanda*100 . ' % <br/>' .
    'Valoare Dobanda: ' . number_format($total-$credit-$com_lunar*$perioada,2,",",""). ' Lei <br/>' .
    'Comision Analiza: ' . $com_analiza . ' Lei <br/>' .
    'Comision Lunar: ' . $com_lunar . ' Lei <br/>' .
    'DAE: ' . $dae . ' % <br/>' .
    'Total: ' . $total . ' Lei <br/>' .
    'Data: ' . date('Y-m-d H:i:s')
;

/*$mail->send();*/
echo 1;


/*header('Location: '.$_SERVER["HTTP_REFERER"]."?s=1");
die();*/

?>