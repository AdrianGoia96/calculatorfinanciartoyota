var pret_glob = 0;
var curs_valutar = 0;
var dobanda = 0;
var com_analiza = 0;
var com_lunar = 0;
var rata = 0;
var perioada = 0;
var credit = 0;
var avans = 0;
var model = "";
var echipare = "";
var ecobon = 0;
var valoare_avans = 0;
var dae = 0;
var total = 0;

$( document ).ready(function() {
    model = "Yaris";
    echipare = "Terra";
    $.ajax({
        type: 'GET',
        url: 'ajax.php',
        data: {action:"check_Curs"},
        dataType: 'text',
        success: function (output) {

        }
    });
    $.ajax({
        type: 'GET',
        url: 'ajax.php',
        data: {action:"pret", model: model, echipare: echipare},
        dataType: 'text',
        success: function (pret) {
            pret_glob = pret;
            getData(model);
            $("#model-dropdown").val(model+"_"+echipare);
        }
    });
});


function updateCalculator(e) {
    model = e.options[e.selectedIndex].value;
    model = model.split("_");
    echipare = model[1];
    model = model[0];
    $.ajax({
        type: 'GET',
        url: 'ajax.php',
        data: {model: model, echipare:echipare , action: "pret"},
        dataType:'text',
        success: function(pret) {
            pret_glob = pret;
            getData(model);
        }
    });

}

function getData(model) {
    $.ajax({
        type: 'get',
        url: 'ajax.php',
        data: {action: "variabile"},
        dataType:'json',
        success: function(output) {
            for(var i = 0; i < output.length ; i++){
                if(output[i].nume == "dobanda"){
                    dobanda = parseFloat(output[i].valoare);
                }
                if(output[i].nume == "com_lunar"){
                    com_lunar = parseFloat(output[i].valoare);
                }
                if(output[i].nume == "com_analiza"){
                    com_analiza = parseFloat(output[i].valoare);
                }
                if(output[i].nume == "curs_valutar"){
                    curs_valutar = parseFloat(output[i].valoare);
                }
                if(model != "rav4"){
                    if(output[i].nume.indexOf("ecobon_non_rav")>=0){
                        ecobon = parseFloat(output[i].valoare);
                    }
                    if(output[i].nume.indexOf("avans_non_rav")>=0){
                        avans = parseFloat(output[i].valoare);
                    }
                }else{
                    if(output[i].nume.indexOf("ecobon_rav")>=0){
                        ecobon = parseFloat(output[i].valoare);
                    }
                    if(output[i].nume.indexOf("avans_rav")>=0){
                        avans = parseFloat(output[i].valoare);
                    }
                }
            }

            var min = Math.round(pret_glob*avans);
            var procent_avans_input = (model === "RAV 4")? 27 : 20;
            var avans_value = Math.round(pret_glob*procent_avans_input/100);
            var max = Math.round(pret_glob*0.5);
            document.getElementById("avans-min").innerHTML = min.toLocaleString('de-DE',{ maximumFractionDigits: 0}) + " &euro;";
            document.getElementById("avans-max").innerHTML = max.toLocaleString('de-DE',{ maximumFractionDigits: 0}) + " &euro;";
            document.getElementById("input-pret-show").setAttribute("min", min);
            document.getElementById("input-pret-show").setAttribute("max", max);

            document.getElementById("input-pret-show").value = avans_value;

            document.getElementById("input-pret").setAttribute("min", avans*100);
            document.getElementById("input-pret").setAttribute("max", 50);
            document.getElementById("input-pret").value = procent_avans_input;

            document.getElementById("input-perioada-show").innerHTML = 60 + " Luni";
            perioada = document.getElementById("input-perioada").value = 60;

            updatePret();
        }
    });
}

function updateAvans() {
    valoare_avans = Math.round(document.getElementById("input-pret").value*pret_glob/100);
    document.getElementById("input-pret-show").value = valoare_avans.toFixed(0);
    updatePret();
}

function updatePerioada() {
    perioada = document.getElementById("input-perioada").value;
    document.getElementById("input-perioada-show").innerHTML = perioada + " Luni";
    updatePret();
}

function updatePret(){
    avans = parseFloat(document.getElementById("input-pret-show").value)/pret_glob;
    var avans_calcul = (parseFloat(document.getElementById("input-pret-show").value) + parseFloat(ecobon/curs_valutar))/pret_glob;
    document.getElementById("input-pret").value = Math.round(avans*100);

    credit = (pret_glob*curs_valutar)*(1-avans_calcul) + com_analiza;
    rata = (pmt(dobanda/12,perioada,-credit)+com_lunar);

    document.getElementById("rata").innerHTML = rata.toLocaleString('de-DE',{ maximumFractionDigits: 2}) + " LEI";
    document.getElementById("credit").innerHTML = (credit-com_analiza).toLocaleString('de-DE',{ maximumFractionDigits: 2}) + " LEI";

    document.getElementById("comision").innerHTML = com_lunar.toLocaleString('de-DE',{ maximumFractionDigits: 2}) + " LEI";

    total = perioada*rata;
    var rate_calc = fillArray(rata,parseInt(perioada)+1);
    rate_calc[0] = -(credit-com_analiza);
    var term_1 = 1+IRR(rate_calc,1/12);
    dae = (Math.pow(term_1,12)-1)*100;

    document.getElementById("DAE").innerHTML = dae.toLocaleString('de-DE',{ maximumFractionDigits: 2}) + "%";
    document.getElementById("dobanda").innerHTML = (total-credit-com_lunar*perioada).toLocaleString('de-DE',{ maximumFractionDigits: 2}) + " LEI";
    document.getElementById("total").innerHTML = total.toLocaleString('de-DE',{ maximumFractionDigits: 2}) + " LEI";
}

function fillArray(value, len) {
    if (len == 0) return [];
    var a = [value];
    while (a.length * 2 <= len) a = a.concat(a);
    if (a.length < len) a = a.concat(a.slice(0, len - a.length));
    return a;
}


$("form").on("submit",function (e) {
    e.preventDefault();
    var model = $("#model-dropdown").val();
    var dealer = $("#dealer-dropdown").val();
    var nume = $("#nume-prenume").val();
    var telefon = $("#telefon").val();
    var email = $("#email").val();
    var peste18 = ($("#peste-18").val() == "on") ? 1 : 0;
    var acord = ($("#de-acord-prim-info-toyota").val() == "on") ? 1 : 0;
    var avans_euro = pret_glob*avans;
    if(model && avans_euro && perioada && rata && credit && dobanda && com_analiza && dae && total && dealer && nume && email && telefon && peste18 && acord && com_lunar){
        $.ajax({
            type: 'get',
            url: 'query.php',
            data: {model: model,avans_euro: avans_euro, perioada: perioada, rata: rata, credit: credit, dobanda: dobanda, com_analiza: com_analiza, dae: dae, total: total, dealer: dealer,nume: nume,telefon: telefon,email: email,peste18: peste18,acord: acord, com_lunar: com_lunar},
            dataType:'text',
            success: function(output) {
                console.log(output);
                if(output == 1){
                    $("#notificare").fadeIn(3000);
                    $("#notificare").fadeOut(1500);
                }else{
                    $("#problema").fadeIn(3000);
                    $("#problema").fadeOut(1500);
                }

                //Facebook Pixel Code

                !function(f,b,e,v,n,t,s)
                {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
                    n.queue=[];t=b.createElement(e);t.async=!0;
                    t.src=v;s=b.getElementsByTagName(e)[0];
                    s.parentNode.insertBefore(t,s)}(window, document,'script',
                    'https://connect.facebook.net/en_US/fbevents.js');
                fbq('init', '887632601363385');
                fbq('track', 'PageView');
                fbq('track', 'Lead');

                var image_1 = new Image(1, 1);
                image_1.src = "//www.facebook.com/tr?id=887632601363385&ev=PageView&noscript=1";

                //(c) 2000-2018 Gemius SA version 2.0
                (_gdeaq = window._gdeaq || []).push(['pageView', 'gdero', '1_aVJq_CCejiSWbyGrVARNT4LaANgc7s7lMwEL3i3br.s7', 'rogde.adocean.pl', '.i2hZ9ZczOmBEIwoJChq9cgmTn_IGFs.7wYRoOVs8w3.y7']);
                $.getScript('//gdero.hit.gemius.pl/gdejs/xgde.js');

                //Google Code


                var google_conversion_id = 971110856;
                var google_conversion_label = "D9Z4COn_nnsQyPOHzwM";
                var google_remarketing_only = false;

                $.getScript('//www.googleadservices.com/pagead/conversion.js');

                var image_2 = new Image(1, 1);
                image_2.src = "//www.googleadservices.com/pagead/conversion/971110856/?label=D9Z4COn_nnsQyPOHzwM&amp;guid=ON&amp;script=0";

                /* Conversion Tracking End */
            }
        })
    }
});

function showDetails(e) {
    if($(e).parent().parent().find("div.overlay").css("display") === 'none'){
        $(e).parent().parent().find("div.overlay").css("display","block");
        $(e).parent().parent().find("div.overlay").css("width",$(e).parent().css("width"));
    }else{
        $(e).parent().parent().find("div.overlay").css("display","none");
    }
}

function pmt(rate, nper, pv, fv, type) {
    if (!fv) fv = 0;
    if (!type) type = 0;

    if (rate == 0) return -(pv + fv)/nper;

    var pvif = Math.pow(1 + rate, nper);
    var pmt = rate / (pvif - 1) * -(pv * pvif + fv);

    if (type == 1) {
        pmt /= (1 + rate);
    };

    return pmt;
}

function IRR(values, guess) {
    // Credits: algorithm inspired by Apache OpenOffice

    // Calculates the resulting amount
    var irrResult = function(values, dates, rate) {
        var r = rate + 1;
        var result = values[0];
        for (var i = 1; i < values.length; i++) {
            result += values[i] / Math.pow(r, (dates[i] - dates[0]) / 365);
        }
        return result;
    }

    // Calculates the first derivation
    var irrResultDeriv = function(values, dates, rate) {
        var r = rate + 1;
        var result = 0;
        for (var i = 1; i < values.length; i++) {
            var frac = (dates[i] - dates[0]) / 365;
            result -= frac * values[i] / Math.pow(r, frac + 1);
        }
        return result;
    }

    // Initialize dates and check that values contains at least one positive value and one negative value
    var dates = [];
    var positive = false;
    var negative = false;
    for (var i = 0; i < values.length; i++) {
        dates[i] = (i === 0) ? 0 : dates[i - 1] + 365;
        if (values[i] > 0) positive = true;
        if (values[i] < 0) negative = true;
    }

    // Return error if values does not contain at least one positive value and one negative value
    /*if (!positive || !negative) return '#NUM!';*/

    // Initialize guess and resultRate
    var guess = (typeof guess === 'undefined') ? 0.1 : guess;
    var resultRate = guess;

    // Set maximum epsilon for end of iteration
    var epsMax = 1e-10;

    // Set maximum number of iterations
    var iterMax = 50;

    // Implement Newton's method
    var newRate, epsRate, resultValue;
    var iteration = 0;
    var contLoop = true;
    do {
        resultValue = irrResult(values, dates, resultRate);
        newRate = resultRate - resultValue / irrResultDeriv(values, dates, resultRate);
        epsRate = Math.abs(newRate - resultRate);
        resultRate = newRate;
        contLoop = (epsRate > epsMax) && (Math.abs(resultValue) > epsMax);
    } while(contLoop && (++iteration < iterMax));

    if(contLoop) return '#NUM!';

    // Return internal rate of return
    return resultRate;
}