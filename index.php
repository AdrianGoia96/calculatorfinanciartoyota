<?php @include "prices.php";?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Calculator Financiar Toyota</title>
    <meta charset="utf-8">
    <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' >
    <meta http-equiv="x-ua-compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<div class="container-fluid">
    <div class="row slider-for" id="header">
        <a href="#solicitare">
            <img src="img/Toyota-HP-Banner-1920-x-800.jpg" alt="Alege programul rabla" id="header-img">
        </a>
    </div>
</div>
<div class="container">
    <div class="row" id="beneficii">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 beneficii-container">
            <div class="row">
                <div class=" col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <p class="beneficii-label">
                        Beneficii
                    </p>
                </div>
                <div class=" col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3" >
                        <img src="img/prima-casare-icon.png" class="beneficii-img" style="margin-bottom: 34px;">
                        <p class="beneficii-details">
                            6.500 RON<br>
                            Prima de casare
                        </p>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                        <img src="img/eco-bon-1700-icon.png" class="beneficii-img"  style="margin-bottom: 28px;">
                        <p class="beneficii-details">
                            1.700 RON<br>
                            Ecobonusul pentru orice model Toyota Hibrid
                        </p>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                        <img src="img/eco-bon-1000-icon.png" class="beneficii-img"  style="margin-bottom: 29px;">
                        <p class="beneficii-details">
                            1.000 RON<br>
                            Ecobonusul pentru emisii CO2 sub 98g/km
                        </p>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                        <img src="img/6-ani-icon.png" class="beneficii-img"  style="margin-bottom: 24px;">
                        <p class="beneficii-details">
                            6 ani sau 200.000 km <br>
                            Garantie
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row" id="modele">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 models-label">
            <p class="title-generic">
                Descoperă modelele hibride Toyota
            </p>
            <p class="title-generic">
                și alege-l pe cel potrivit
            </p>
        </div>
    </div>
    <div class="row">
        <div class="container">
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 card-big" id="yaris">
                <div class="card-masina">
                    <p class="car-model">Yaris Hybrid</p>
                    <div class="car-img-container">
                        <a href="https://www.toyota.ro/new-cars/yaris"><img src="img/yaris-hybrid.png" class="card-car-img"></a>
                    </div>
                    <div class="tx-c">
                        <a href="https://www.toyota.ro/new-cars/yaris" class="btn-get-model">Afla mai multe detalii</a>
                    </div>
                    <div class="tx-c">
                        <button class="btn-get-car-details" onclick="showDetails(this);">
                            <span class="icon-sd">+</span>
                        </button>
                    </div>
                    <div class="overlay">
                        <div class="overlay-contents">
                            <p><span class="icon-motor"></span>Motorizare - <b>1.5 litri Hybrid e-CVT</b></p>
                            <p><span class="icon-consum"></span>Consum de combustibil mixt (l/100km) - <b>3.3 l/100km</b></p>
                            <p><span class="icon-caroserie"></span>Tip caroserie - <b>Hatchback 5 usi</b></p>
                            <p><span class="icon-emisii"></span>Emisii CO2 (g/km) de la - <b>75 g/km</b></p>
                            <p class="pret-container">
                                Incepand de la:
                                <span class="pret"> <?php echo number_format ($masini["Yaris"]["Terra"],0,",","." );?> &euro;</span>
                            </p>
                            <div class="tx-c">
                                <button class="btn-solicita">Solicita o oferta</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 card-big" id="auris">
                <div class="card-masina">
                    <p class="car-model">Auris Hybrid</p>
                    <div class="car-img-container">
                        <a href="https://www.toyota.ro/new-cars/auris"> <img src="img/auris-hybrid.png" class="card-car-img"></a>
                    </div>
                    <div class="tx-c">
                        <a href="https://www.toyota.ro/new-cars/auris" class="btn-get-model">Afla mai multe detalii</a>
                    </div>
                    <div class="tx-c">
                        <button class="btn-get-car-details" onclick="showDetails(this);">
                            <span class="icon-sd">+</span>
                        </button>
                    </div>
                    <div class="overlay">
                        <div class="overlay-contents">
                            <p><span class="icon-motor"></span>Motorizare - <b> 1.8 litri Hybrid e-CVT</b></p>
                            <p><span class="icon-consum"></span>Consum de combustibil mixt (l/100km) - <b>3.5 l/100km</b></p>
                            <p><span class="icon-caroserie"></span>Tip caroserie - <b> Hatchback 5 usi </b></p>
                            <p><span class="icon-emisii"></span>Emisii CO2 (g/km) de la - <b>79 g/km</b>/p>
                            <p class="pret-container">
                                Incepand de la:
                                <span class="pret"> <?php echo number_format ($masini["Auris"]["Terra"],0,",","." );?> &euro;</span>
                            </p>
                            <div class="tx-c">
                                <button class="btn-solicita">Solicita o oferta</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 card-big" id="aurists">
                <div class="card-masina">
                    <p class="car-model">Auris Touring Sports Hybrid</p>
                    <div class="car-img-container">
                        <a href="https://www.toyota.ro/new-cars/auris-touring-sports" ><img src="img/auris-touring.png" class="card-car-img"></a>
                    </div>
                    <div class="tx-c">
                        <a href="https://www.toyota.ro/new-cars/auris-touring-sports" class="btn-get-model">Afla mai multe detalii</a>
                    </div>
                    <div class="tx-c">
                        <button class="btn-get-car-details" onclick="showDetails(this);">
                            <span class="icon-sd">+</span>
                        </button>
                    </div>
                    <div class="overlay">
                        <div class="overlay-contents">
                            <p><span class="icon-motor"></span>Motorizare - <b> 1.8 litri Hybrid e-CVT</b></p>
                            <p><span class="icon-consum"></span>Consum de combustibil mixt (l/100km) - <b>3.6 l/100km</b></p>
                            <p><span class="icon-caroserie"></span>Tip caroserie - <b> Break 5 usi</b></p>
                            <p><span class="icon-emisii"></span>Emisii CO2 (g/km) de la - <b>83 g/km</b></p>
                            <p class="pret-container">
                                Incepand de la:
                                <span class="pret"> <?php echo number_format ($masini["Auris-TS"]["Luna"],0,",","." );?> &euro;</span>
                            </p>
                            <div class="tx-c">
                                <button class="btn-solicita">Solicita o oferta</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 card-big" id="chr">
                <div class="card-masina">
                    <p class="car-model">Toyota C-HR Hybrid</p>
                    <div class="car-img-container">
                        <a href="https://www.toyota.ro/new-cars/c-hr" ><img src="img/chr.png" class="card-car-img"></a>
                    </div>
                    <div class="tx-c">
                        <a href="https://www.toyota.ro/new-cars/c-hr" class="btn-get-model">Afla mai multe detalii</a>
                    </div>
                    <div class="tx-c">
                        <button class="btn-get-car-details" onclick="showDetails(this);">
                            <span class="icon-sd">+</span>
                        </button>
                    </div>
                    <div class="overlay">
                        <div class="overlay-contents">
                            <p><span class="icon-motor"></span>Motorizare - <b> 1.8 litri hybrid e-CVT</b></p>
                            <p><span class="icon-consum"></span>Consum de combustibil mixt (l/100km) - <b>3,8 l/100km</b></p>
                            <p><span class="icon-caroserie"></span>Tip caroserie - <b> SUV compact</b></p>
                            <p><span class="icon-emisii"></span>Emisii CO2 (g/km) de la - <b>86 g/km</b></p>
                            <p class="pret-container">
                                Incepand de la:
                                <span class="pret"> <?php echo number_format ($masini["C-HR"]["C-enter"],0,",","." );?> &euro;</span>
                            </p>
                            <div class="tx-c">
                                <button class="btn-solicita">Solicita o oferta</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 card-big" id="rav4">
                <div class="card-masina">
                    <p class="car-model">Rav 4 Hybrid</p>
                    <div class="car-img-container">
                        <a href="https://www.toyota.ro/new-cars/rav4" ><img src="img/rav4.png" class="card-car-img"></a>
                    </div>
                    <div class="tx-c">
                        <a href="https://www.toyota.ro/new-cars/rav4" class="btn-get-model">Afla mai multe detalii</a>
                    </div>
                    <div class="tx-c">
                        <button class="btn-get-car-details" onclick="showDetails(this);">
                            <span class="icon-sd">+</span>
                        </button>
                    </div>
                    <div class="overlay">
                        <div class="overlay-contents">
                            <p><span class="icon-motor"></span>Motorizare - <b> 2.5 litri hybrid e-CVT</b></p>
                            <p><span class="icon-consum"></span>Consum de combustibil mixt (l/100km) - <b>4.9 l/100km</b></p>
                            <p><span class="icon-caroserie"></span>Tip caroserie - <b> SUV Compact</b></p>
                            <p><span class="icon-emisii"></span>Emisii CO2 (g/km) de la - <b>115 g/km</b></p>
                            <p class="pret-container">
                                Incepand de la:
                                <span class="pret"> <?php echo number_format ($masini["RAV 4"]["Executive 4x2"],0,",","." );?> &euro;</span>
                            </p>
                            <div class="tx-c">
                                <button class="btn-solicita">Solicita o oferta</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 card-big" id="prius">
                <div class="card-masina">
                    <p class="car-model">Prius Hybrid</p>
                    <div class="car-img-container">
                        <a href="https://www.toyota.ro/new-cars/prius"><img src="img/prius.jpg" class="card-car-img"></a>
                    </div>
                    <div class="tx-c">
                        <a href="https://www.toyota.ro/new-cars/prius" class="btn-get-model">Afla mai multe detalii</a>
                    </div>
                    <div class="tx-c">
                        <button class="btn-get-car-details" onclick="showDetails(this);">
                            <span class="icon-sd">+</span>
                        </button>
                    </div>
                    <div class="overlay">
                        <div class="overlay-contents">
                            <p><span class="icon-motor"></span>Motorizare - <b> 1.8 litri Hybrid e-CVT</b></p>
                            <p><span class="icon-consum"></span>Consum de combustibil mixt (l/100km) - <b>3,2 l/100km</b></p>
                            <p><span class="icon-caroserie"></span>Tip caroserie - <b> Liftback 5 usi </b></p>
                            <p><span class="icon-emisii"></span>Emisii CO2 (g/km) de la - <b>79 g/km</b></p>
                            <p class="pret-container">
                                Incepand de la:
                                <span class="pret"> <?php echo number_format ($masini["Prius"]["Terra"],0,",","." );?> &euro;</span>
                            </p>
                            <div class="tx-c">
                                <button class="btn-solicita">Solicita o oferta</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row form-container" id="solicitare">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h2 class="title-generic">Solicită o ofertă</h2>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="display: none;color: green;" id="notificare">
            <h3>Solicitarea a fost trimisa!</h3>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="display: none;color: red;" id="problema">
            <h3>Ne pare rau, dar solicitarea nu poate fi procesata.</h3>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <form id="solicitare-cerere" name="solicitare-cerere">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 input-container">
                <select name="model-dropdown" id="model-dropdown" class="form-inputs choose-model dropdown" form="solicitare-cerere" onchange="updateCalculator(this);" required>
                    <!--<option value="">Alege un model</option>-->
                    <?php
                    $i = 0;
                    $denumiri = array_keys($masini);
                    foreach($masini as $model){
                        foreach($model as $echipare => $pret){
                            echo "<option value='{$denumiri[$i]}_{$echipare}'> {$denumiri[$i]} - {$echipare}</option>";
                        }
                        $i++;
                    }
                    ?>
                </select>
            </div>
            <div class="row slider-row" id="slider-row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <h3>Valoare avans</h3>
                    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9 slider-container">
                        <span id="avans-min"> &euro;</span>
                        <span id="avans-max" class="pull-right"> &euro;</span>
                        <input type="range" min="10" max="50" value="20" class="slider" id="input-pret" oninput="updateAvans();" onchange="updateAvans();">
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <input type="number" class="input-number" id="input-pret-show" value="" min="" max="" onchange="updatePret();">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <h3>Durata creditului</h3>
                    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9 slider-container">
                        <span>12 Luni</span>
                        <span class="pull-right">60 Luni</span>
                        <input type="range" min="12" max="60" value="60" class="slider" id="input-perioada" oninput="updatePerioada();" onchange="updatePerioada();">
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <span class="input-number" disabled id="input-perioada-show">60 Luni</span>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 details-container">
                    <div class="col-xs-4 col-sm-2 col-md-2 col-lg-2">
                        <p class="no-margin">Rată</p>
                        <p id="rata"> LEI</p>
                    </div>
                    <div class="col-xs-4 col-sm-2 col-md-2 col-lg-2">
                        <p class="no-margin">Credit</p>
                        <p id="credit"> LEI</p>
                    </div>
                    <div class="col-xs-4 col-sm-2 col-md-2 col-lg-2">
                        <p class="no-margin">Dobandă</p>
                        <p id="dobanda"> LEI</p>
                    </div>
                    <div class="col-xs-4 col-sm-2 col-md-2 col-lg-2">
                        <p class="no-margin">Comision Lunar</p>
                        <p id="comision"> LEI</p>
                    </div>
                    <div class="col-xs-4 col-sm-2 col-md-2 col-lg-2">
                        <p class="no-margin">DAE</p>
                        <p id="DAE">%</p>
                    </div>
                    <div class="col-xs-4 col-sm-2 col-md-2 col-lg-2">
                        <p class="no-margin">Total</p>
                        <p id="total" style="color: #1fb4e1">LEI</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 input-container">
                            <select name="dealer-dropdown" id="dealer-dropdown" class="form-inputs dropdown" form="solicitare-cerere" required>
                                <option value="">Alege un dealer</option>
                                <option value="TOYOTA BACAU - Auto Best Center">TOYOTA BACAU - Auto Best Center</option>
                                <option value="TOYOTA BAIA MARE - Land Motors">TOYOTA BAIA MARE - Land Motors</option>
                                <option value="TOYOTA BISTRITA - Mondial Motors">TOYOTA BISTRITA - Mondial Motors</option>
                                <option value="TOYOTA BRASOV - Motors Management">TOYOTA BRASOV - Motors Management</option>
                                <option value="TOYOTA BRAILA - Estima Motors">TOYOTA BRAILA - Estima Motors</option>
                                <option value="TOYOTA BUCURESTI NORD - Inchcape Motors">TOYOTA BUCURESTI NORD - Inchcape Motors</option>
                                <option value="TOYOTA BUCURESTI SUD -Toyo Motor Center">TOYOTA BUCURESTI SUD -Toyo Motor Center</option>
                                <option value="TOYOTA BUCURESTI VEST - Kaizen Auto">TOYOTA BUCURESTI VEST - Kaizen Auto</option>
                                <option value="TOYOTA BUZAU - Rol Car Motors">TOYOTA BUZAU - Rol Car Motors</option>
                                <option value="TOYOTA CLUJ NAPOCA - Profi Auto">TOYOTA CLUJ NAPOCA - Profi Auto</option>
                                <option value="TOYOTA CONSTANTA - Motor Expert">TOYOTA CONSTANTA - Motor Expert</option>
                                <option value="TOYOTA CRAIOVA - Top Serv Motors">TOYOTA CRAIOVA - Top Serv Motors</option>
                                <option value="TOYOTA FOCSANI - Auto Best Center">TOYOTA FOCSANI - Auto Best Center</option>
                                <option value="TOYOTA IASI - Mega Auto">TOYOTA IASI - Mega Auto</option>
                                <option value="TOYOTA ODORHEI - Euro Car Trading">TOYOTA ODORHEI - Euro Car Trading</option>
                                <option value="TOYOTA ORADEA - Car Select Mobility">TOYOTA ORADEA - Car Select Mobility</option>
                                <option value="TOYOTA PITESTI - Next Automobile">TOYOTA PITESTI - Next Automobile</option>
                                <option value="TOYOTA PLOIESTI - Arena Auto">TOYOTA PLOIESTI - Arena Auto</option>
                                <option value="TOYOTA RAMNICU VALCEA - Next Automobile">TOYOTA RAMNICU VALCEA - Next Automobile</option>
                                <option value="TOYOTA SIBIU - Son Motors">TOYOTA SIBIU - Son Motors</option>
                                <option value="TOYOTA SUCEAVA - Revlaco Motors">TOYOTA SUCEAVA - Revlaco Motors</option>
                                <option value="TOYOTA TARGU MURES - Euro Car Trading">TOYOTA TARGU MURES - Euro Car Trading</option>
                                <option value="TOYOTA TIMISOARA - New Wavemotors">TOYOTA TIMISOARA - New Wavemotors</option>
                            </select>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 input-container">
                            <input type="text" name="nume-prenume" id="nume-prenume" placeholder="Nume si prenume" class=" form-inputs" form="solicitare-cerere" required>
                        </div>
                    </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 input-container">
                        <input type="email" name="email" id="email" placeholder="Email" class=" form-inputs" form="solicitare-cerere" required>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 input-container">
                        <input type="text" name="telefon" id="telefon" placeholder="Telefon" class=" form-inputs" form="solicitare-cerere" required>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top: 10px;">
                <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <p>*Toate campurile sunt obligatorii</p>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <input type="checkbox" name="peste-18" id="peste-18" form="solicitare-cerere" required> <b> Am peste 18 ani.</b>
                    </div>
                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                        <input type="checkbox" name="de-acord-prim-info-toyota" id="de-acord-prim-info-toyota" form="solicitare-cerere" required> <b>Sunt de acord sa primesc informatii despre produsele si serviciile Toyota</b>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 tx-c sol-of">
                        <input type="submit" value="Solicita Oferta" id="solicita-oferta-submit">
                    </div>
                </div>
                </form>
            </div>
            <div class="row disclaimer">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <p class="disclaimer-content">
                        *Ofertă valabilă în limita stocului disponibil, începând cu intrarea în vigoare a Ghidului 2018 de finanțare PSIPAN derulat de AFM și finanțat din Fondul de Mediu, sub rezerva acceptării Toyota România ca producător validat în program și a îndeplinirii de către client a tuturor condițiilor impuse de Ghidul PSIPAN 2018.Beneficiile programului Rabla sunt prezentate conform ultimei informări AFM. Toyota România nu iși asumă responsabilitatea cu privire la eventuala modificare a acestor beneficii de către AFM.
                        <br><br>
                        Declar ca sunt de acord ca TOYOTA ROMÂNIA S.R.L., persoană juridică română, cu sediul în Voluntari, B-dul. Pipera nr. 1, Județul Ilfov, înregistrată la Registrul Comerțului București sub nr.J23/2402/2008, CUI RO12723443, cod operator de date cu caracter personal nr. 2485, să folosească datele cu caracter personal obținute prin completarea formularului on-line din cadrul acestui site în scopul îmbunătățirii și promovării serviciilor și produselor companiei noastre, precum și ale grupului de societăți care comercializează autoturisme și piese de schimb marca Toyota, prin realizarea unor sondaje de măsurare a gradului de satisfacție a clienților, precum și prin mijloace de marketing direct. Am fost informat că am dreptul să mă opun prelucrării datelor personale care mă privesc și să solicitat ștergerea lor. Pentru exercitarea acestor drepturi, mă pot adresa cu o cerere scrisă, datată și semnată, la adresa Toyota Romania SRL, Voluntari, B-dul. Pipera nr. 1, Județul Ilfov, în atenția departamentului Customer Satisfaction.
                        <br><br>
                        Yaris Hybrid Terra - 12.900 Euro (tva inclus) rata 1.001,37 RON
                        <br>
                        Oferta financiara este adresata exclusiv persoanelor fizice si este realizata de UniCredit Consumer Financing pentru modelul Toyota Yaris, in valoare de 14,878 Euro (pret 69,183 Lei la un curs exemplificativ de [4,65] Lei/1 Euro) TVA inclus, avans 33,415 Lei. Valoarea totala estimativa a Creditului Toyota Fix fara CPI va fi de 35,768 Lei pe o perioada de 60 luni, astfel: rata anuala dobanda 8.49%, analiza dosar 790 Lei, administrare lunara credit 39 Lei, rata lunara estimativa 789 Lei, valoarea totala estimativa platibila a creditului 47,332.4 Lei, DAE 12.26%. Valoarea estimativa a ratei lunare de mai sus nu include asigurarea optionala de viata si asigurarea CASCO.
                        <br><br>
                        Auris Hybrid Terra -  17.400 Euro (tva inclus) rata 1.344,74 RON
                        <br>
                        Oferta financiara este adresata exclusiv persoanelor fizice si este realizata de UniCredit Consumer Financing pentru modelul Toyota Auris, in valoare de 19,378 Euro (pret 90,108 Lei la un curs exemplificativ de [4,65] Lei/1 Euro) TVA inclus, avans 44,343 Lei. Valoarea totala estimativa a Creditului Toyota Fix fara CPI va fi de 45,765 Lei pe o perioada de 60 luni, astfel: rata anuala dobanda 8.49%, analiza dosar 790 Lei, administrare lunara credit 39 Lei, rata lunara estimativa 994 Lei, valoarea totala estimativa platibila a creditului 59,636 Lei, DAE 11.52%. Valoarea estimativa a ratei lunare de mai sus nu include asigurarea optionala de viata si asigurarea CASCO.
                        <br><br>
                        Valoarea creditului nu poate depasi suma de 100.000 RON.
                        <br>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <a href="#header" id="footer-btn" >
            SUS
        </a>
    </div>
    </div>
</div>
<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script src="js/main.js"></script>
</body>
</html>